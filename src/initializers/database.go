package initializers

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

var DB *gorm.DB
var RC *redis.Client

func ConnectDB(config *Config) {

	var err error

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		config.DBHost, config.DBUserName, config.DBUserPassword, config.DBName, config.DBPort)

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Could not connect to the initializers: %v", err)
	}

	log.Println("Connected to the initializers successfully")
}

func InitRedis(config *Config) {
	RC = redis.NewClient(&redis.Options{
		Addr: config.RedisHost + ":" + config.RedisPort,
	})

	ctx := context.Background()
	_, err := RC.Ping(ctx).Result()
	if err != nil {
		panic(err)
	}
}
