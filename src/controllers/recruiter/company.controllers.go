// src/controllers/recruiter/company.controllers.go

package recruiter

import (
	"TechHunterClone/src/models"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
	"gorm.io/gorm"
	"net/http"
	"time"
)

type CompanyController struct {
	DB          *gorm.DB
	RedisClient *redis.Client
}

func NewCompanyController(DB *gorm.DB, RC *redis.Client) CompanyController {
	return CompanyController{DB: DB, RedisClient: RC}
}

// GetCompanies godoc
// @Summary Get all companies
// @Description Retrieve a list of all companies.
// @Tags Company
// @Produce json
// @Success 200 {array} models.Company
// @Failure 500 "Internal Server Error"
// @Router /api/company/companies [get]
func (cc *CompanyController) GetCompanies(c *gin.Context) {
	var companies []models.Company
	redisKey := "companiesList"

	ctx := c.Request.Context()

	val, err := cc.RedisClient.Get(ctx, redisKey).Result()
	if err == redis.Nil {
		if err := cc.DB.Find(&companies).Error; err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		jsonData, err := json.Marshal(companies)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		cc.RedisClient.Set(ctx, redisKey, jsonData, time.Hour)
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	} else {
		err := json.Unmarshal([]byte(val), &companies)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, companies)
}

// GetCompany godoc
// @Summary Get a single company
// @Description Retrieve details of a specific company by ID.
// @Tags Company
// @Produce json
// @Param id path int true "Company ID"
// @Success 200 {object} models.Company
// @Failure 404 "Company not found"
// @Router /api/company/{id} [get]
func (cc *CompanyController) GetCompany(c *gin.Context) {
	var company models.Company
	id := c.Param("id")

	if err := cc.DB.First(&company, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Company not found"})
		return
	}

	c.JSON(http.StatusOK, company)

}

// CreateCompany godoc
// @Summary Create a new company
// @Description Create a new company with the given details.
// @Tags Company
// @Accept json
// @Produce json
// @Param company body models.Company true "Company details"
// @Success 201 {object} models.Company
// @Failure 400 "Bad Request"
// @Failure 500 "Internal Server Error"
// @Router /api/company/create [post]
func (cc *CompanyController) CreateCompany(c *gin.Context) {
	var company models.Company
	if err := c.BindJSON(&company); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := cc.DB.Create(&company).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, company)
}

// UpdateCompany godoc
// @Summary Update an existing company
// @Description Update details of an existing company by ID.
// @Tags Company
// @Accept json
// @Produce json
// @Param id path int true "Company ID"
// @Param company body models.Company true "Updated Company Information"
// @Success 200 "Company updated successfully"
// @Failure 400 "Bad Request"
// @Failure 404 "Company not found"
// @Router /api/company/update/{id} [put]
func (cc *CompanyController) UpdateCompany(c *gin.Context) {
	var company models.Company
	id := c.Param("id")

	if err := cc.DB.First(&company, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Company not found"})
		return
	}

	if err := c.BindJSON(&company); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	cc.DB.Save(&company)

	c.JSON(http.StatusOK, company)
}

// DeleteCompany godoc
// @Summary Delete a company
// @Description Delete an existing company by ID.
// @Tags Company
// @Produce json
// @Param id path int true "Company ID"
// @Success 200 "Company deleted successfully"
// @Failure 404 "Company not found"
// @Router /api/company/delete/{id} [delete]
func (cc *CompanyController) DeleteCompany(c *gin.Context) {
	id := c.Param("id")

	if err := cc.DB.Delete(&models.Company{}, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Company not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Company deleted"})
}

// GetCompaniesVacancy godoc
// @Summary Get Top companies
// @Description Get Top companies and their vacancies.
// @Tags Company
// @Produce json
// @Param id path int true "Company ID"
// @Success 200 "Company deleted successfully"
// @Failure 404 "Company or vacancies not found"
// @Router /api/company/company-vacancy [get]
func (cc *CompanyController) GetCompaniesVacancy(c *gin.Context) {
	var companies []models.TopCompany
	redisKey := "topCompaniesWithVacancies"
	ctx := c.Request.Context()

	val, err := cc.RedisClient.Get(ctx, redisKey).Result()
	if err == redis.Nil {
		if err := cc.DB.Preload("TopVacancies").Find(&companies).Error; err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		jsonData, err := json.Marshal(companies)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		cc.RedisClient.Set(ctx, redisKey, jsonData, time.Hour)
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	} else {
		err := json.Unmarshal([]byte(val), &companies)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, companies)
}
