// src/controllers/recruiter/vacancy.controllers.go

package recruiter

import (
	"TechHunterClone/src/models"
	services "TechHunterClone/src/services/auth_service"
	"encoding/json"
	"fmt"
	"github.com/IBM/sarama"
	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
	"gorm.io/gorm"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"
)

var kafkaProducer sarama.SyncProducer

type VacancyController struct {
	DB          *gorm.DB
	RedisClient *redis.Client
}

func NewVacancyController(DB *gorm.DB, RC *redis.Client) VacancyController {
	return VacancyController{DB: DB, RedisClient: RC}
}

func (vc *VacancyController) GetVacancy(c *gin.Context) {
	vacancyId := c.Param("vacancyId")
	var vacancy models.Vacancy

	if err := vc.DB.First(&vacancy, vacancyId).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Vacancy not found"})
		return
	}

	c.JSON(http.StatusOK, vacancy)
}

// GetVacancies godoc
// @Summary Get all vacancies with pagination
// @Description Retrieve a paginated list of all vacancies.
// @Tags Vacancy
// @Produce json
// @Param page query int false "Page number"
// @Param pageSize query int false "Number of items per page"
// @Success 200 {object} map[string]interface{} "List of vacancies with pagination details"
// @Failure 500 "Internal Server Error"
// @Router /api/vacancy/vacancies  [get]
func (vc *VacancyController) GetVacancies(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	pageSize, _ := strconv.Atoi(c.DefaultQuery("pageSize", "10"))
	if page < 1 {
		page = 1
	}
	if pageSize < 1 {
		pageSize = 10
	}

	offset := (page - 1) * pageSize
	var vacancies []models.Vacancy

	redisKey := fmt.Sprintf("vacanciesPage:%d:Size:%d", page, pageSize)
	totalCountKey := "vacanciesTotalCount"
	ctx := c.Request.Context()

	val, err := vc.RedisClient.Get(ctx, redisKey).Result()
	if err == redis.Nil {
		if err := vc.DB.Offset(offset).Limit(pageSize).Find(&vacancies).Error; err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		jsonData, err := json.Marshal(vacancies)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		vc.RedisClient.Set(ctx, redisKey, jsonData, time.Hour)

		var totalCount int64
		vc.DB.Model(&models.Vacancy{}).Count(&totalCount)
		vc.RedisClient.Set(ctx, totalCountKey, totalCount, time.Hour)

		totalPages := int(math.Ceil(float64(totalCount) / float64(pageSize)))
		c.JSON(http.StatusOK, gin.H{
			"vacancies":   vacancies,
			"totalPages":  totalPages,
			"currentPage": page,
		})
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	} else {
		err := json.Unmarshal([]byte(val), &vacancies)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		totalCountVal, err := vc.RedisClient.Get(ctx, totalCountKey).Result()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		totalCount, _ := strconv.ParseInt(totalCountVal, 10, 64)
		totalPages := int(math.Ceil(float64(totalCount) / float64(pageSize)))

		c.JSON(http.StatusOK, gin.H{
			"vacancies":   vacancies,
			"totalPages":  totalPages,
			"currentPage": page,
		})
	}
}

// CreateVacancy godoc
// @Summary Create a new vacancy
// @Description Create a new vacancy with the given details.
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param vacancy body models.Vacancy true "Vacancy Details"
// @Success 201 {object} models.Vacancy
// @Failure 400 "Bad Request"
// @Failure 401 "Unauthorized"
// @Failure 500 "Server Error"
// @Router /api/vacancy/create [post]
func (vc *VacancyController) CreateVacancy(c *gin.Context) {
	var vacancy models.Vacancy

	if err := c.BindJSON(&vacancy); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid vacancy data", "details": err.Error()})
		return
	}

	tokenString, err := c.Cookie("Authorise")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Authorization required: No token found", "details": err.Error()})
		return
	}

	userID, err := services.GetID(tokenString)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed: Invalid token"})
		return
	}

	vacancy.AuthorID = userID

	if err := vc.DB.Create(&vacancy).Error; err == nil {
		vacancyJSON, jsonErr := json.Marshal(vacancy)
		if jsonErr != nil {
			log.Printf("Error serializing vacancy data: %v", jsonErr)
		}

		_, _, kafkaErr := kafkaProducer.SendMessage(&sarama.ProducerMessage{
			Topic: "vacancy-topic",
			Value: sarama.ByteEncoder(vacancyJSON),
		})

		if kafkaErr != nil {
			log.Printf("Error sending message to Kafka: %v", kafkaErr)
		}
	}
}

// UpdateVacancy godoc
// @Summary Update an existing vacancy
// @Description Update details of an existing vacancy by ID.
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param id path int true "Vacancy ID"
// @Param vacancy body models.Vacancy true "Updated Vacancy Information"
// @Success 200 "Vacancy updated successfully"
// @Failure 400 "Bad Request"
// @Failure 404 "Vacancy not found"
// @Router /api/vacancy/{id} [put]
func (vc *VacancyController) UpdateVacancy(c *gin.Context) {
	var vacancy models.Vacancy
	id := c.Param("id")

	if err := vc.DB.First(&vacancy, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Vacancy not found"})
		return
	}

	if err := c.BindJSON(&vacancy); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	vc.DB.Save(&vacancy)

	c.JSON(http.StatusOK, vacancy)
}

// DeleteVacancy godoc
// @Summary Delete a vacancy
// @Description Delete an existing vacancy by ID.
// @Tags Vacancy
// @Produce json
// @Param id path int true "Vacancy ID"
// @Success 200 "Vacancy deleted successfully"
// @Failure 404 "Vacancy not found"
// @Router /api/vacancy/delete/{id} [delete]
func (vc *VacancyController) DeleteVacancy(c *gin.Context) {
	id := c.Param("id")

	if err := vc.DB.Delete(&models.Vacancy{}, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Vacancy not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Vacancy deleted"})

}

//func (vc *VacancyController) SubscribeVacancies(c *gin.Context) {
//	tokenString, err := c.Cookie("Authorise")
//	if err != nil {
//		c.JSON(http.StatusBadRequest, gin.H{"error": "Authorization required: No token found", "details": err.Error()})
//		return
//	}
//
//	userID, err := services.GetID(tokenString)
//	if err != nil {
//		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed: Invalid token"})
//		return
//	}
//
//	var currentUser models.User
//	if err := vc.DB.First(&currentUser, userID).Error; err != nil {
//		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
//		return
//	}
//
//	var subscriptionRequest struct {
//		IsSubscribed bool `json:"is_subscribed"`
//	}
//	if err := c.BindJSON(&subscriptionRequest); err != nil {
//		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})
//		return
//	}
//
//	currentUser.IsSubscribed = subscriptionRequest.IsSubscribed
//	if err := vc.DB.Save(&currentUser).Error; err != nil {
//		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update subscription status"})
//		return
//	}
//
//	status := "unsubscribed"
//	if currentUser.IsSubscribed {
//		status = "subscribed"
//	}
//	c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("Successfully %s to vacancies.", status)})
//}
