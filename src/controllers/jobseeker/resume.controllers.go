// src/controllers/jobseeker/resume.controllers.go

package jobseeker

import (
	"TechHunterClone/src/initializers"
	"TechHunterClone/src/models"
	services "TechHunterClone/src/services/auth_service"
	"gorm.io/gorm"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ResumeController struct {
	DB *gorm.DB
}

func NewResumeController(DB *gorm.DB) ResumeController {
	return ResumeController{DB}
}

// CreateResume godoc
// @Summary Create a new resume
// @Description Creates a new resume for a job seeker.
// @Tags Resume
// @Accept json
// @Produce json
// @Success 201 {object} map[string]interface{} "Resume created successfully with resumeID"
// @Failure 400 "Invalid resume data"
// @Failure 401 "Authentication required: No token found or Invalid token"
// @Failure 500 "Failed to create resume"
// @Router /api/resume/create [post]
func (rc *ResumeController) CreateResume(c *gin.Context) {
	var resume models.Resume

	if err := c.BindJSON(&resume); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid resume data", "details": err.Error()})
		return
	}

	tokenString, err := c.Cookie("Authorise")
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication required: No token found"})
		return
	}

	userID, err := services.GetID(tokenString)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed: Invalid token"})
		return
	}
	resume.UserID = userID

	if err := rc.DB.Create(&resume).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create resume", "details": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "Resume created successfully", "resumeID": resume.ID})
}

// UpdateResume godoc
// @Summary Update an existing resume
// @Description Updates an existing resume for a job seeker.
// @Tags Resume
// @Accept  json
// @Produce json
// @Param id path int true "Resume ID"
// @Param resume body models.Resume true "Updated Resume Information"
// @Success 200 {object} models.Resume "Resume updated successfully"
// @Failure 400 "Invalid input"
// @Failure 404 "Resume not found"
// @Router /api/resume/update/{id} [put]
func (rc *ResumeController) UpdateResume(c *gin.Context) {
	var resume models.Resume
	id := c.Param("id")

	if err := initializers.DB.First(&resume, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Resume not found"})
		return
	}

	if err := c.BindJSON(&resume); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	rc.DB.Save(&resume)

	c.JSON(http.StatusOK, resume)
}

// DeleteResume godoc
// @Summary Delete a resume
// @Description Deletes a resume for a job seeker.
// @Tags Resume
// @Produce json
// @Param id path int true "Resume ID"
// @Success 200 {object} map[string]string "Resume deleted"
// @Failure 404 "Resume not found"
// @Router /api/resume/{id} [delete]
func (rc *ResumeController) DeleteResume(c *gin.Context) {
	id := c.Param("id")

	if err := rc.DB.Delete(&models.Resume{}, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Resume not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Resume deleted"})
}
