package controllers

import (
	"TechHunterClone/src/initializers"
	"TechHunterClone/src/models"
	"TechHunterClone/src/services/auth_service"
	emailservice "TechHunterClone/src/services/mail"
	"TechHunterClone/src/services/tokenutil"
	"errors"
	"github.com/thanhpk/randstr"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"strings"
)

func errorResponse(c *gin.Context, status int, err error) {
	c.AbortWithError(status, err)
}

type AuthController struct {
	DB *gorm.DB
}

func NewAuthController(DB *gorm.DB) AuthController {
	return AuthController{DB}
}

// SignUpUser godoc
// @Summary SignUpUser/Registration
// @Description Registers a new user with their basic information and role.
// @Tags Authentication
// @Accept  x-www-form-urlencoded
// @Produce json
// @Param first_name formData string true "User's first name"
// @Param last_name formData string true "User's last name"
// @Param email formData string true "User's email address"
// @Param password formData string true "User's password"
// @Param role formData string true "Role of the user (user/recruiter)"
// @Success 200 {object} models.User
// @Failure 409 {object} object "User with that email already exists"
// @Failure 502 {object} object "Internal server error"
// @Router /api/auth/register [post]
func (ac *AuthController) SignUpUser(c *gin.Context) {

	firstName := c.PostForm("first_name")
	lastName := c.PostForm("last_name")
	email := c.PostForm("email")
	password := c.PostForm("password")
	roleStr := c.PostForm("role")

	var role models.Role
	switch roleStr {
	case "user":
		role = models.RoleUser
	case "recruiter":
		role = models.RoleRecruiter
	}

	hashedPassword, err := authservice.HashPassword(password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errors.New("failed to hash password"))
		return
	}

	newUser := &models.User{
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Password:  hashedPassword,
		Role:      role,
	}

	result := ac.DB.Create(&newUser)

	if result.Error != nil && strings.Contains(result.Error.Error(), "duplicate key value violates unique") {
		c.JSON(http.StatusConflict, gin.H{"status": "fail", "message": "User with that mail already exists"})
		return

	} else if result.Error != nil {
		c.JSON(http.StatusBadGateway, gin.H{"status": "error", "message": "Something bad happened"})
		return
	}

	config, _ := initializers.LoadConfig(".")

	// Generate Verification Code
	code := randstr.String(20)

	verification_code := tokenservice.Encode(code)

	newUser.VerificationCode = verification_code
	ac.DB.Save(newUser)

	if strings.Contains(firstName, " ") {
		firstName = strings.Split(firstName, " ")[1]
	}

	// ? Send Email
	emailData := models.EmailData{
		URL:       config.ClientOrigin + "/verify-email/" + code,
		FirstName: firstName,
		Subject:   "Your account verification code",
	}

	emailservice.SendEmail(newUser, &emailData)

	message := "We sent an mail with a verification code to " + newUser.Email
	c.JSON(http.StatusCreated, gin.H{"status": "success", "message": message})
}

// SignInUser godoc
// @Summary Logs in a user
// @Description Logs in a user with their email and password.
// @Tags Authentication
// @Accept  x-www-form-urlencoded
// @Produce json
// @Param email formData string true "User's email address"
// @Param password formData string true "User's password"
// @Success 302 "User logged in successfully and redirected"
// @Failure 401 "Invalid email or password"
// @Failure 403 "User email not verified"
// @Router /api/auth/login [post]
func (ac *AuthController) SignInUser(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")

	var user models.User
	if err := ac.DB.Where("email = ?", email).First(&user).Error; err != nil {
		errorResponse(c, http.StatusUnauthorized, errors.New("invalid mail or password"))
		return
	}

	if !user.Verified {
		c.JSON(http.StatusForbidden, gin.H{"status": "fail", "message": "Please verify your email"})
		return
	}

	if err := authservice.VerifyPassword(user.Password, password); err != nil {
		errorResponse(c, http.StatusUnauthorized, errors.New("invalid email or password"))
		return
	}

	// Generate Token
	accessToken, err := tokenservice.CreateToken(&user)
	if err != nil {
		errorResponse(c, http.StatusInternalServerError, err)
		return
	}

	c.SetCookie("Authorise", accessToken, int(time.Hour.Seconds()*48), "/", "localhost", false, true)
	c.Redirect(http.StatusFound, "/api/main")
}

// LogoutUser godoc
// @Summary Logs out a user
// @Description Logs out a user by clearing their session cookies.
// @Tags Authentication
// @Produce json
// @Success 302 "User logged out successfully and redirected"
// @Router /api/auth/logout [post]
func (ac *AuthController) LogoutUser(c *gin.Context) {
	c.SetCookie("Authorise", "", -1, "/", "localhost", false, true)
	c.Redirect(http.StatusFound, "/api/main")
}

// VerifyEmail godoc
// @Summary Verifies user's email
// @Description Verifies a user's email using the verification code sent to their email.
// @Tags Authentication
// @Accept  x-www-form-urlencoded
// @Produce json
// @Param verificationCode path string true "The verification code sent to the user's email"
// @Success 200 "Email verified successfully"
// @Failure 400 "Invalid verification code or user does not exist"
// @Failure 409 "User already verified"
// @Router /api/auth/verify-email/{verificationCode} [get]
func (ac *AuthController) VerifyEmail(c *gin.Context) {

	code := c.Params.ByName("verificationCode")
	verificationCode := tokenservice.Encode(code)

	var updatedUser models.User
	result := ac.DB.First(&updatedUser, "verification_code = ?", verificationCode)
	if result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": "fail", "message": "Invalid verification code or user doesn't exists"})
		return
	}

	if updatedUser.Verified {
		c.JSON(http.StatusConflict, gin.H{"status": "fail", "message": "User already verified"})
		return
	}

	updatedUser.VerificationCode = ""
	updatedUser.Verified = true
	ac.DB.Save(&updatedUser)

	c.JSON(http.StatusOK, gin.H{"status": "success", "message": "Email verified successfully"})

	c.Redirect(http.StatusFound, "/api/auth/login")

}
