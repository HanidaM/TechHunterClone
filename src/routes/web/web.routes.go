package web

import (
	"TechHunterClone/src/initializers"
	"TechHunterClone/src/models"
	tokenutil "TechHunterClone/src/services/tokenutil"
	"errors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

type RouteController struct{}

func NewWebRouteController() RouteController {
	return RouteController{}
}

func (wrc RouteController) RegisterWebRoutes(rg *gin.RouterGroup) {
	rg.GET("/auth/login", wrc.login)
	rg.GET("/auth/register", wrc.signup)
	rg.GET("/main", wrc.main)
	rg.GET("/main/profile/user", wrc.userProfile)
	rg.GET("/main/profile/recruiter", wrc.recruiterProfile)
	rg.GET("/main/companies", wrc.companiesList)
}

func (wrc RouteController) login(c *gin.Context) {
	c.HTML(http.StatusOK, "login.html", gin.H{})
}

func (wrc RouteController) signup(c *gin.Context) {
	c.HTML(http.StatusOK, "signup.html", gin.H{})
}

func (wrc RouteController) main(c *gin.Context) {
	token, err := c.Cookie("Authorise")
	var loggedIn bool
	var role string
	if err == nil {
		user, err := tokenutil.ValidateToken(token)
		if err == nil {
			loggedIn = true
			role = string(user.Role)
		}
	}

	c.HTML(http.StatusOK, "main.html", gin.H{
		"LoggedIn": loggedIn,
		"Role":     role,
	})
}

func (wrc RouteController) userProfile(c *gin.Context) {
	token, err := c.Cookie("Authorise")
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "tokenutil not found"})
		return
	}
	user, err := tokenutil.ValidateToken(token)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "unauthorized - invalid tokenutil"})
		return
	}

	var resume models.Resume
	resumeExists := true
	if err := initializers.DB.Where("user_id = ?", user.ID).First(&resume).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			resumeExists = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "internal server error"})
			return
		}
	}

	c.HTML(http.StatusOK, "userpage.html", gin.H{
		"ResumeExists":   resumeExists,
		"Title":          resume.Title,
		"Experience":     resume.Experience,
		"Grading":        resume.Grading,
		"Education":      resume.Education,
		"Skills":         resume.Skills,
		"Projects":       resume.Projects,
		"SocialNetworks": resume.SocialNetworks,
	})
}

func (wrc RouteController) recruiterProfile(c *gin.Context) {
	token, err := c.Cookie("Authorise")
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "tokenutil not found"})
		return
	}
	user, err := tokenutil.ValidateToken(token)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "unauthorized - invalid tokenutil"})
		return
	}

	var vacancy models.Vacancy
	vacancyExists := true
	if err := initializers.DB.Where("author_id = ?", user.ID).First(&vacancy).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			vacancyExists = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "internal server error"})
			return
		}
	}

	c.HTML(http.StatusOK, "recruiter.html", gin.H{
		"VacancyExists": vacancyExists,
		"JobTitle":      vacancy.JobTitle,
		"Company":       vacancy.CompanyName,
		"Salary":        vacancy.Salary,
		"Location":      vacancy.Location,
		"TechStacks":    vacancy.TechStacks,
		"VacancyURL":    vacancy.VacancyURL,
		"Source":        vacancy.Source,
	})
}
func (wrc RouteController) companiesList(c *gin.Context) {
	c.HTML(http.StatusOK, "companies.html", gin.H{})
}
