package authroutes

import (
	controllers "TechHunterClone/src/controllers/auth"
	"github.com/gin-gonic/gin"
)

type AuthRouteController struct {
	authController controllers.AuthController
}

func NewAuthRouteController(authController controllers.AuthController) AuthRouteController {
	return AuthRouteController{authController}
}

func (arc *AuthRouteController) AuthRoute(rg *gin.RouterGroup) {
	router := rg.Group("/auth")

	router.POST("/register", arc.authController.SignUpUser)
	router.POST("/login", arc.authController.SignInUser)
	router.POST("/logout", arc.authController.LogoutUser)
	router.GET("/verify-email/:verificationCode", arc.authController.VerifyEmail)
}
