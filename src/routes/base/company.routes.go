package base

import (
	"TechHunterClone/src/controllers/recruiter"
	"github.com/gin-gonic/gin"
)

type CompanyRouteController struct {
	companyController recruiter.CompanyController
}

func NewCompanyRouteController(companyController recruiter.CompanyController) CompanyRouteController {
	return CompanyRouteController{companyController}
}

func (crc *CompanyRouteController) CompanyRoute(rg *gin.RouterGroup) {

	router := rg.Group("company")
	router.GET("/companies", crc.companyController.GetCompanies)
	router.GET("/:id", crc.companyController.GetCompany)
	router.POST("/create", crc.companyController.CreateCompany)
	router.PUT("/update/:id", crc.companyController.UpdateCompany)
	router.DELETE("/delete/:id", crc.companyController.DeleteCompany)
	router.GET("/company-vacancy", crc.companyController.GetCompaniesVacancy)
}
