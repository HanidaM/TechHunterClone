package base

import (
	"TechHunterClone/src/controllers/recruiter"

	"github.com/gin-gonic/gin"
)

type VacancyRouteController struct {
	vacancyController recruiter.VacancyController
}

func NewVacancyRouteController(vacancyController recruiter.VacancyController) VacancyRouteController {
	return VacancyRouteController{vacancyController}
}

func (vc *VacancyRouteController) VacancyRoute(rg *gin.RouterGroup) {
	r := rg.Group("/vacancy")

	r.GET("/:id", vc.vacancyController.DeleteVacancy)
	r.GET("/vacancies", vc.vacancyController.GetVacancies)
	r.POST("/create", vc.vacancyController.CreateVacancy)
	r.PUT("/update/:id", vc.vacancyController.UpdateVacancy)
	r.DELETE("/delete/:id", vc.vacancyController.DeleteVacancy)
	//r.PUT("/subscribe", vc.vacancyController.SubscribeVacancies)

}
