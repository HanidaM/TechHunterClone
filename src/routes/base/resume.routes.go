package base

import (
	"TechHunterClone/src/controllers/jobseeker"
	"github.com/gin-gonic/gin"
)

type ResumeRouteController struct {
	resumeController jobseeker.ResumeController
}

func NewResumeRouteController(resumeController jobseeker.ResumeController) ResumeRouteController {
	return ResumeRouteController{resumeController}
}

func (rc *ResumeRouteController) ResumeRoute(rg *gin.RouterGroup) {
	router := rg.Group("/resume")

	//r.GET("/", rc.resumeController.GetResumes)
	//r.GET("/:id", rc.resumeController.GetResume)
	router.POST("/create", rc.resumeController.CreateResume)
	router.PUT("/update/:id", rc.resumeController.UpdateResume)
	router.DELETE("/:id", rc.resumeController.DeleteResume)
}
