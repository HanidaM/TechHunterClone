package kafka

//import (
//	"TechHunterClone/src/initializers"
//	emailservice "TechHunterClone/src/services/mail"
//	"bytes"
//	"crypto/tls"
//	"encoding/json"
//	"github.com/k3a/html2text"
//	"gopkg.in/gomail.v2"
//	"log"
//
//	"TechHunterClone/src/models"
//	"github.com/IBM/sarama"
//)
//
//type KafkaConsumer struct {
//	Consumer sarama.Consumer
//}
//
//func NewKafkaConsumer(brokerList []string) *KafkaConsumer {
//	consumer, err := sarama.NewConsumer(brokerList, nil)
//	if err != nil {
//		log.Fatalf("Failed to start Kafka consumer: %v", err)
//	}
//	return &KafkaConsumer{Consumer: consumer}
//}
//
//func (vc *KafkaConsumer) ConsumeMessages(topic string) {
//	partitionConsumer, err := vc.Consumer.ConsumePartition(topic, 0, sarama.OffsetNewest)
//	if err != nil {
//		log.Fatalf("Failed to start Kafka partition consumer: %v", err)
//	}
//	defer partitionConsumer.Close()
//
//	log.Printf("Consumer started for topic %s", topic)
//	for {
//		select {
//		case msg := <-partitionConsumer.Messages():
//			log.Printf("Received a message: %s", string(msg.Value))
//			var vacancy models.Vacancy
//			if err := json.Unmarshal(msg.Value, &vacancy); err != nil {
//				log.Printf("Error unmarshalling vacancy data: %v", err)
//				continue
//			}
//
//			processVacancy(&vacancy)
//		}
//	}
//}
//
//func processVacancy(vacancy *models.Vacancy) {
//	subscribedUsers := FetchSubscribedUsers()
//	for _, user := range subscribedUsers {
//		SendVacancyEmail(&user, vacancy)
//	}
//}
//
//func FetchSubscribedUsers() []models.User {
//	var subscribedUsers []models.User
//	initializers.DB.Where("is_subscribed = ?", true).Find(&subscribedUsers)
//	return subscribedUsers
//}
//
//func SendVacancyEmail(user *models.User, vacancy *models.Vacancy) {
//	config, err := initializers.LoadConfig(".")
//	if err != nil {
//		log.Fatalf("Could not load config: %v", err)
//	}
//
//	emailData := models.EmailData{
//		Subject: "New Vacancy Posted: " + vacancy.JobTitle,
//	}
//
//	var body bytes.Buffer
//	template, err := emailservice.ParseTemplateDir("src/templates")
//	if err != nil {
//		log.Fatalf("Could not parse template: %v", err)
//	}
//
//	if err := template.ExecuteTemplate(&body, "vacancyNotification.html", emailData); err != nil {
//		log.Fatalf("Error executing template: %v", err)
//	}
//
//	m := gomail.NewMessage()
//	m.SetHeader("From", config.EmailFrom)
//	m.SetHeader("To", user.Email)
//	m.SetHeader("Subject", emailData.Subject)
//	m.SetBody("text/html", body.String())
//	m.AddAlternative("text/plain", html2text.HTML2Text(body.String()))
//
//	d := gomail.NewDialer(config.SMTPHost, config.SMTPPort, config.SMTPUser, config.SMTPPass)
//	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
//
//	if err := d.DialAndSend(m); err != nil {
//		log.Printf("Could not send mail to %s: %v", user.Email, err)
//	}
//}
