package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"
)

type Vacancy struct {
	ID          uint `gorm:"primarykey"`
	AuthorID    uint `json:"author_id"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	JobTitle    string     `json:"job_title" gorm:"type:text"`
	CompanyName string     `json:"company_name" gorm:"type:text"`
	Salary      string     `json:"salary" `
	Location    string     `json:"location" gorm:"type:text"`
	TechStacks  StringList `json:"tech_stacks" gorm:"type:text"`
	VacancyURL  string     `json:"vacancy_url" gorm:"type:text"`
	Source      string     `json:"source" gorm:"type:text"`
}

type Company struct {
	ID          uint `gorm:"primarykey"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	Name        string     `json:"name" gorm:"type:varchar(255);unique;not null"`
	Description string     `json:"description" gorm:"type:text"`
	Headcount   int        `json:"headcount"`
	Type        string     `json:"type" gorm:"type:varchar(255)"`
	Industry    string     `json:"industry" gorm:"type:varchar(255)"`
	LogoURL     string     `json:"logo_url" gorm:"type:text"`
	WebsiteURL  string     `json:"website_url" gorm:"type:text"`
	TechStacks  StringList `json:"tech_stacks" gorm:"type:jsonb"`
}

type TopCompany struct {
	ID           uint `gorm:"primarykey"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	Name         string       `json:"name" gorm:"type:varchar(255);unique;not null"`
	Description  string       `json:"description" gorm:"type:text"`
	Headcount    int          `json:"headcount"`
	Type         string       `json:"type" gorm:"type:varchar(255)"`
	Industry     string       `json:"industry" gorm:"type:varchar(255)"`
	LogoURL      string       `json:"logo_url" gorm:"type:text"`
	WebsiteURL   string       `json:"website_url" gorm:"type:text"`
	TopVacancies []TopVacancy `json:"top_vacancies" gorm:"foreignKey:CompanyName;references:Name"`
}

type TopVacancy struct {
	ID          uint `gorm:"primarykey"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	JobTitle    string `json:"job_title" gorm:"type:varchar(255)"`
	CompanyName string `json:"company_name" gorm:"type:varchar(255)"`
	Salary      string `json:"salary" gorm:"type:varchar(255)"`
	Location    string `json:"location" gorm:"type:varchar(255)"`
	VacancyURL  string `json:"vacancy_url" gorm:"type:text"`
}

func (list *StringList) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New("failed to unmarshal JSON value")
	}

	return json.Unmarshal(bytes, list)
}

func (list StringList) Value() (driver.Value, error) {
	return json.Marshal(list)
}
