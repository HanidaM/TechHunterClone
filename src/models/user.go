package models

import (
	"time"
)

type Role string

const (
	RoleUser      Role = "user"
	RoleRecruiter Role = "recruiter"
)

type User struct {
	ID               uint `gorm:"primary"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	FirstName        string `json:"first_name" gorm:"index;type:varchar(30)" validate:"required,min=2,max=30"`
	LastName         string `json:"last_name" gorm:"index;type:varchar(30)" validate:"required,min=2,max=30"`
	Email            string `json:"mail" gorm:"uniqueIndex;type:varchar(100)" validate:"required,mail"`
	Password         string `json:"-"`
	Role             Role   `json:"role" gorm:"index;type:varchar(20)"`
	VerificationCode string
	Verified         bool `gorm:"not null"`
	IsSubscribed     bool `json:"is_subscribed"`
}

type EmailData struct {
	URL       string
	FirstName string
	Subject   string
}

type Resume struct {
	ID             uint `gorm:"primarykey"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
	Title          string     `json:"title" gorm:"type:varchar(100)"`
	UserID         uint       `json:"user_id" gorm:"not null;index"` // Foreign key
	Experience     StringList `json:"experience" gorm:"type:text"`
	Grading        string     `json:"grading" gorm:"type:text"`
	Education      StringList `json:"education" gorm:"type:text"`
	Skills         StringList `json:"skills" gorm:"type:text"`
	Projects       StringList `json:"projects" gorm:"type:text"`
	SocialNetworks StringList `json:"social_networks" gorm:"type:text"`
}

type StringList []string
