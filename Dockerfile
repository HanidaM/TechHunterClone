FROM golang:1.21.3-alpine3.17 as builder
WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o ./src ./main.go


FROM alpine:3.14 AS runner
WORKDIR /app
COPY --from=builder /app/src .
EXPOSE 8080
ENTRYPOINT ["./main"]