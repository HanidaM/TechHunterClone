package main

import (
	_ "TechHunterClone/docs"
	"TechHunterClone/src/controllers/auth"
	"TechHunterClone/src/controllers/jobseeker"
	"TechHunterClone/src/controllers/recruiter"
	"TechHunterClone/src/initializers"
	authroutes "TechHunterClone/src/routes/auth"
	"TechHunterClone/src/routes/base"
	"TechHunterClone/src/routes/web"
	"context"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	config, err := initializers.LoadConfig(".")
	if err != nil {
		log.Fatalf("Could not load environment variables: %v", err)
	}
	initializers.ConnectDB(&config)
	initializers.InitRedis(&config)

	server := gin.Default()
	initControllersAndRoutes(server)

	//server.Use(middlewares.PrometheusMetrics())
	//middlewares.RegisterPrometheus(server)

	//
	//brokers := []string{"localhost:9092"} // Update with actual Kafka brokers
	//kafkaConsumer := kafka.NewKafkaConsumer(brokers)
	//go kafkaConsumer.ConsumeMessages("vacancy-topic")

	server.LoadHTMLGlob("src/templates/*.html")

	srv := &http.Server{
		Addr:    ":8080",
		Handler: server,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	<-signalChan

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")
}

func initControllersAndRoutes(server *gin.Engine) {
	authController := controllers.NewAuthController(initializers.DB)
	companyController := recruiter.NewCompanyController(initializers.DB, initializers.RC)
	resumeController := jobseeker.NewResumeController(initializers.DB)
	vacancyController := recruiter.NewVacancyController(initializers.DB, initializers.RC)

	router := server.Group("/api")
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	controller := authroutes.NewAuthRouteController(authController)
	controller.AuthRoute(router)
	routeController := base.NewCompanyRouteController(companyController)
	routeController.CompanyRoute(router)
	resumeRouteController := base.NewResumeRouteController(resumeController)
	resumeRouteController.ResumeRoute(router)
	vacancyRouteController := base.NewVacancyRouteController(vacancyController)
	vacancyRouteController.VacancyRoute(router)
	webRouteController := web.NewWebRouteController()
	webRouteController.RegisterWebRoutes(router)

}
