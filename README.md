
# TechHunterClone

A brief description of the project

This project is a job searching platform that connects job seekers with recruiters. It allows job seekers to find and apply for jobs, while recruiters can post job vacancies and manage applications.



## Tech Stack

- **Programming Language:** Go
- **Web Framework:** Gin
- **ORM:** Gorm
- **Database:** PostgreSQL
- **Caching:** Redis
- **API Documentation:** Swagger
- **Configuration Management:** Viper
- **Front-end Scripting:** jQuery, AJAX\
- **Front-end Framework:** Bootstrap 5



## Project Structure

Below is a high-level overview of the project structure:

Structure described below is known as a "Modular Directory Structure" or "Feature-based Directory Structure"
```
User
├───docs
│   # Documentation files, including API docs, project guides, and references
└───src
    ├───controllers
    │   ├───auth
    │   │   # Controllers for handling authentication processes
    │   ├───jobseeker
    │   │   # Controllers for jobseeker-related operations
    │   └───recruiter
    │       # Controllers for recruiter-related operations
    ├───initializers
    │   # Scripts and configurations for initializing the application
    ├───middlewares
    │   # Middleware functions for request processing, like authentication and logging
    ├───models
    │   # Data models defining the structure for various entities in the application
    ├───routes
    │   ├───auth
    │   │   # Routes related to authentication processes
    │   ├───base
    │   │   # Base routes for the application
    │   └───web
    │       # Routes for serving web pages and handling web requests
    ├───services
    │   ├───auth_service
    │   │   # Services related to authentication logic
    │   ├───mail
    │   │   # Services for sending and managing emails
    │   └───tokenutil
    │       # Utilities for handling tokens (like JWTs)
    └───templates
        # HTML templates for the web frontend



```
